$(function() {
	$("#file_1").on("change", function () {
		$("#i_score").text("-");
		$("#i_score_bar").css("width", "0%");
		if($("#file_1").get(0).files[0]) {
			var reader1  = new FileReader();
			var file1 = $("#file_1").get(0).files[0];
			reader1.addEventListener("load", function () {
	    		$("#image_1").attr("src", reader1.result);
	  		}, false);
	  		if (file1) {
	    		reader1.readAsDataURL(file1);
	  		}
		}
	});
	$("#file_2").on("change", function () {
		$("#i_score").text("-");
		$("#i_score_bar").css("width", "0%");
		if($("#file_2").get(0).files[0]) {
			var reader2  = new FileReader();
			var file2 = $("#file_2").get(0).files[0];
			reader2.addEventListener("load", function () {
	    		$("#image_2").attr("src", reader2.result);
	  		}, false);
	  		if (file2) {
	    		reader2.readAsDataURL(file2);
	  		}
		}
	});
	$("#form_1").on("submit", function () {
		if($("#file_1").get(0).files[0] && $("#file_2").get(0).files[0]) {
			$("#b_compare").addClass("disabled");
			$("#i_score_bar").removeClass("determinate");
			$("#i_score_bar").addClass("indeterminate");
			var data = {};
			data.image_1 = $("#image_1").attr("src").substring("data:image/png;base64,".length);
			data.image_2 = $("#image_2").attr("src").substring("data:image/png;base64,".length);
			$.post("http://02srv008d0.hosts.eni.intranet:8080/biometrics/1.0/faces/compare", JSON.stringify(data), function(retval) {
				var score = retval.score
				if(score == -1.0) {
					score = 0.0
					alert("face not detected");
				} else if(score < 0.0) {
					score = 0;
				} else if(score > 1.0) {
					score = 1.0;
				}
				score = Math.round(score * 100.0);
				$("#i_score").text(score + " %");
				$("#i_score_bar").css("width", score + "%");
				$("#b_compare").removeClass("disabled");
				$("#i_score_bar").removeClass("indeterminate");
				$("#i_score_bar").addClass("determinate");
			}, dataType="json");
	  	}
		return false;
	});
});
